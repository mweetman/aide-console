This project provides a central console for running aide, using Ansible for the heavy lifting. Aide databases from managed hosts are copied to the control node for storage and comparison operations.

### Configuration and Use
```
[me@host ~]$ git clone https://gitlab.com/mweetman/aide-console.git
...output omitted...
[me@host ~]$ cd aide-console
[me@host aide-console]$ vim ./ansible.cfg 
[me@host aide-console]$ ./aide-console --add-host <host>
...output omitted...
[me@host aide-console]$ ./aide-console --check <host>
...output omitted...
[me@host aide-console]$ ./aide-console --update <host>
...output omitted...
[me@host aide-console]$ ./aide-console --compare <host>
[0] 20200127T110141
[1] 20200127T111256
[2] 20200127T121758
[3] 20200127T122022
[4] 20200127T132429
Select the first database: 3
Select the second database: 4
Start timestamp: 2020-01-27 13:35:49 +1100 (AIDE 0.16)
AIDE found differences between the two databases!!
...output omitted...
```

### TODO
1. Clean up code
2. Add support for operations on multiple hosts


